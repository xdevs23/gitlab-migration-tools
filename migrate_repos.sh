#!/bin/bash

set -euo pipefail

export GITLAB_SERVER=https://git.halogenos.org

export GITLAB_GROUP_ID=108 # halogenOS


cd /mnt/full_big_block/gitea/repos/halogenos/


for repo in *; do

        echo "Migrating $repo"
        echo " > Checking repo details..."
        project_id=$(curl "$GITLAB_SERVER/api/v4/groups/$GITLAB_GROUP_ID/projects?search=$(echo $repo | sed -e 's/[.]git$//')" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.[0]["id"]' )
        if [[ $project_id == "null" ]]; then 
                echo " > Creating repository $repo..."
                project_id=$(curl -d "path=$(echo $repo | sed -e 's/[.]git$//')&namespace_id=$GITLAB_GROUP_ID" -X POST "$GITLAB_SERVER/api/v4/projects" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.["id"]')
        fi
        echo " -> Project id: $project_id"

        echo " > Pushing..."
        pushd $repo
        git push --all --force git@git.halogenos.org:halogenOS/$repo
        git push --tags git@git.halogenos.org:halogenOS/$repo
        popd

        echo " > Changing default branch"
        curl "$GITLAB_SERVER/api/v4/projects/$project_id" -X PUT -H "PRIVATE-TOKEN: $GITLAB_TOKEN" --data "id=$project_id&default_branch=XOS-11.0" >/dev/null
        echo
done

